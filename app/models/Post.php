<?php

Class Post{

private $db;

public function __construct(){
    $this->db = new Database;

}

public function findAllPosts(){
    $this->db->query('SELECT * FROM posts ORDER BY createdat ASC');

    $results = $this->db->resultSet();
    return $results;
}

public function addPost($data){
    $this->db->query("INSERT INTO posts (userid, title, body) VALUES (:userid, :title, :body)");
    $this->db->bind(':userid', $data['userid']);
    $this->db->bind(':title', $data['title']);
    $this->db->bind(':body', $data['body']);

    if($this->db->execute())
      {
          
        return true;
    }
    else
    {
        return false;
    }
}

}