<?php

class Pages extends Controller {
   public function __construct(){
       $this->userModel = $this->model("User");
   }

   
   public function index()
   {
       $data = 
       [
           $title = "Home Page",
       ];

       $this->view ("pages/index", $data);
       
   }

   public function about()
   {
       $data = 
       [
           $title = "Home Page",
       ];

       $this->view ("pages/about", $data);
       
   }
   public function register()
   {
       $data = 
       [
           'username' =>'',
           'email' =>'',
           'password' =>'',
           'confirmPassword' =>'',
           'usernameError' =>'',
           'emailError' =>'',
           'passwordError' =>'',
           'confirmPasswordError' =>'',
       ];

       if($_SERVER['REQUEST_METHOD']=='POST'){
           $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

           //tisztít
           $data = 
           [
               'username' => trim($_POST["username"]),
               'email' => trim($_POST["email"]),
               'password' => trim($_POST["password"]),
               'confirmPassword' => trim($_POST["confirmPassword"]),
               'usernameError' =>'',
               'emailError' =>'',
               'passwordError' =>'',
               'confirmPasswordError' =>'',
    
           ];
           $nameValidation = "/^[a-zA-Z0-9]*$/";
           $passwordValidation ="/^(.{0,7}|[^a-z]*|[^\d]*)$/i"; 
           //név validáció
           

           if(empty($data["username"])){
               $data['usernameError']="Kérem adjon meg egy nevet";
           
            }
           
          elseif($this->userModel->nameExist($data["username"])){
               
               $data['usernameError'] = 'A felhasználónév már foglalt';
            }
            elseif(!preg_match($nameValidation, $data["username"]))
           {
            $data['usernameError']="A név csak alfanumerikus karakterekből állhat";
           }
           
           

           




           //email validáció
           if(empty($data["email"]))
           {
            $data['emailError']="Kérem adjon meg egy nevet";

        }
        elseif(!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
        {
            $data['emailError'] = 'Kérem az emailt a helyes formátumban adja meg';
        }
            //létezik az email

        elseif($this->userModel->emailExist($data['email'])){
              $data['emailError'] = 'Az email cím már foglalt';
            
      }
        //jelszó validáció
      

        if(empty($data["password"]))
        {
         $data['passwordError']="Kérem adjon meg egy jelszavat";

        }
        elseif(!preg_match($passwordValidation, $data["password"]))
        {
         $data['passwordError']='A jelszónak tartalmaznia kell legalább 1 numerikus karaktert';
        }
        elseif(strlen($data["password"])<6)
        {
            $data['passwordError']='A jelszónak minimum 6 karakternek kell lennie';
        }
        if(empty($data["confirmPassword"]))
        {
         $data['confirmPasswordError']="Kérem adja meg újra jelszavát";

        }
        else{
            if($data['password']!=$data["confirmPassword"]){
                $data['confirmPasswordError']='A jelszavak nem egyeznek.';
            }
        }

        //ha minden rendben

        if(empty($data['usernameError']) && empty($data['passwordError']) && empty($data['emailError']) && empty($$data['confirmPasswordError'])){
            //titkosítás

            $data['password'] = password_hash($data['password'], PASSWORD_DEFAULT);
            //regiszt

            if($this->userModel->register($data)){
                header("Location: " . URLROOT . "/pages/login" );
            }else{
                die("Valami hiba történt");
            }

        }

           
           
       }

       $this->view("pages/register", $data);
       
   }

   public function login()
   {

    $data =
    [
        'username' =>'',
        'password' =>'',
        'usernameError' =>'',
        'passwordError' =>'',

    ];

    if($_SERVER['REQUEST_METHOD']=="POST"){
        $_POST = filter_input_array(INPUT_POST , FILTER_SANITIZE_STRING);


    $data =
    [
        'username' => trim($_POST['username']),
        'password' => trim($_POST['password']),
        'usernameError' =>'',
        'passwordError' =>'',

    ];

    if(empty($data["username"])){
        $data['usernameError']="Kérem adja meg a felhasználó nevét";
    
     }
     
    if(empty($data["password"])){
        $data['passwordError']="Kérem adja meg a jelszavát";
    
     }

 //ha minden rendben

 if(empty($data['usernameError']) && empty($data['passwordError'])){

    $loggedinuser = $this->userModel->login($data["username"], $data["password"]);
    
    if($loggedinuser){
        $this->createUserSession($loggedinuser);  
    }
    else
    {
        $data['passwordError']="A jelszó vagy felhasználónév helytelen!";
        $this->view("pages/login", $data);
    }

   }
}
   else
   {
    $data =
    [
        'username' =>'',
        'password' =>'',
        'usernameError' =>'',
        'passwordError' =>'',

    ];

   }
       $this->view("pages/login", $data);  
    

    }

    public function createUserSession($user){
        $_SESSION['userid'] = $user->id;
        $_SESSION['username'] = $user->name;
        $_SESSION['password'] = $user->password;
        header("location: " . URLROOT . "/pages/index");
    }

    public function logout(){
        unset($_SESSION['userid']);
        unset($_SESSION['username']);
        unset($_SESSION['password']);
        header("location: " . URLROOT . "/pages/login");
    }
}



?>