<?php
header('Content-Type: text/html; charset=ISO-8859-1');


class Posts extends Controller {
    public function __construct(){
        $this->postModel = $this->model('Post');
    }

    public function index(){
        $posts = $this->postModel->findAllPosts();
        $data =
        [
            'posts' => $posts
        ];
        $this->view('posts/index', $data);
    }

    public function create(){

        if(!isloggedin()){
            header("location: ".URLROOT. " /posts ");
        }
        $data =
        [
            "userid" => '',
            "title"  => '',
            "body" => '',
            "titleError"  => '',
            "bodyError" => '',
        ];

        if($_SERVER['REQUEST_METHOD'] == 'POST')
        {
           $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
        

        $data =
        [
            "userid" => $_SESSION['userid'],
            "title"  => trim($_POST['title']),
            "body" => trim($_POST['body']),
            "titleError"  => '',
            "bodyError" => '',
        ];

        if(empty($data['title']))
        {
            $data['titleError']=utf8_decode("A cím nem lehet üres.");
        }
        if(empty($data['body']))
        {
            $data['bodyError']=utf8_decode("A mező nem lehet üres.");
        }

        if(empty($data['titleError']) && empty($data['bodyError']))
        {
           if($this->postModel->addPost($data)){
            header("location: ".URLROOT. " /posts ");
        }
        else
        {
            $data['bodyError']="Valami hiba történt.";
           }
           
        }
        else
        {
            $this->view('posts/create', $data);
        }

        
    }
        $this->view('posts/create', $data);
    }
    public function update($id){
        $data =
        [
            "userid" => '',
            "title"  => '',
            "body" => '',
            "titleError"  => '',
            "bodyError" => '',
        ];
        $this->view('posts/update', $data);
    }
    public function delete(){

       $this->view('posts/delete', $data);
        
    }
}