<?php
require APPROOT . '../views/includes/navigation.php';

?>
<?php

require APPROOT . '../views/includes/head.php';

?>
<div class="navbar">

</div>

<div class="container-login">
    <div class="wrapper-login">
        <h2>Bejelentkezés</h2>
        <form action="<?php echo URLROOT; ?>/pages/login" method="POST">

      <input type="text" placeholder="Username *" name="username">
        <span class="invalidFeedback">
            <?php echo $data['usernameError']; ?>
            </span>

     <input type="password" placeholder="Password *" name="password">
        <span class="invalidFeedback">
            <?php echo $data['passwordError']; ?>
            </span>
<button type="submit" id="submit"  value="submit">Belépés</button>
<p class="options">Nincs fiókja?<a href="<?php echo URLROOT; ?>/pages/register">
Regisztráljon!</a></p>
     </form>
    </div>
</div>