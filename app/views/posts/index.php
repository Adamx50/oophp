<?php
header('Content-Type: text/html; charset=ISO-8859-1');
?>

<?php
require APPROOT . '../views/includes/head.php';
?>

<div class="navbar">
<?php
require APPROOT . '../views/includes/navigation.php';
?>

</div>
  

<div class="container">
<?php if (isset($_SESSION['userid'])) : ?>
    <a class="btn btn-success" href=" <?php echo URLROOT; ?>/posts/create" style="margin:50px 0px 50px 0px" >Create</a>
<?php else : ?>
<?php endif; ?>
<?php foreach($data['posts'] as $post): ?>

<div class="container-item">
<?php if(isset($_SESSION['userid']) && $_SESSION['userid']==$post->userid): ?>
  <a class="btn btn-primary" href=" <?php echo URLROOT . "/posts/update/" . $post->id  ?>" >Update</a>
  <?php endif; ?>
<h2>
<?php echo $post->title; ?>
</h2>

<h3 class="date">
<?php echo utf8_decode('Közzétéve: ' . date('F j h:m', strtotime($post->createdat))) ?>
</h3>
<p>
<?php echo $post->body; ?>
</p>
</div>
<?php endforeach; ?>
</div>