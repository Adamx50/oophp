<?php
header('Content-Type: text/html; charset=ISO-8859-1');

?>
<?php

require APPROOT . '../views/includes/head.php';
?>
<div class="navbar">
<?php
require APPROOT . '../views/includes/navigation.php';
?>
</div>

  <div class="container center">

<h1>Új poszt</h1>

<form action="<?php echo URLROOT; ?>/posts/create" method="POST">
<div class="form-item">
<input type="text" name="title" placeholder="cím..." >
<span class="invalidFeedback">
            <?php echo $data['titleError']; ?>
            </span>
</div>

<div class="form-item">
<textarea name="body"  placeholder="poszt..." style="width:400px; height:100px;">

</textarea>
<span class="invalidFeedback">
            <?php echo $data['bodyError']; ?>
            </span>
</div>
<button class="btn btn-success" name="submit" type="submit">Küld</button>


</form>


</div>