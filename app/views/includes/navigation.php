<nav class="top-nav">
    <ul>
        <li>
            <a href="<?php echo URLROOT; ?>/pages/index">Home</a>
        </li>
        <li>
            <a href="<?php echo URLROOT; ?>/pages/about">About</a>
        </li>
        <li>
            <a href="<?php echo URLROOT; ?>/projects">Projects</a>
        </li>
        <li>
            <a href="<?php echo URLROOT; ?>/posts">Blog</a>
        </li>
        <li>
            <a href="<?php echo URLROOT; ?>/contact">Contact</a>
        </li>
        <li class="btn-login">
            <?php if(isset($_SESSION['userid'])) : ?>
                <a href="<?php echo URLROOT; ?>/pages/logout">Logout</a>
                <?php else : ?>
           <a href="<?php echo URLROOT; ?>/pages/logout">Login</a>
           <?php endif; ?>
        </li>
    </ul>
</nav>